import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyC8mQYivKQKkhrNQap77kFTyJ_VHzhRKhE",
    authDomain: "taskgrabbngo.firebaseapp.com",
    databaseURL: "https://taskgrabbngo.firebaseio.com",
    projectId: "taskgrabbngo",
    storageBucket: "taskgrabbngo.appspot.com",
    messagingSenderId: "623696033068",
    appId: "1:623696033068:web:4829a87d391caa0202a889",
    measurementId: "G-704HMJZDVK"
  };

  firebase.initializeApp(firebaseConfig);
  export default firebase;