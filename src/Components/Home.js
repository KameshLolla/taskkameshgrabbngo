import React, { Component } from 'react';
import { Text, TextInput, View, Modal, TouchableOpacity, RefreshControl } from 'react-native';
import { Container, Header, Icon, Footer, Picker } from 'native-base';
import HomeContainer from "../Containers/HomeContainer"
import Toast from 'react-native-easy-toast';
import DraggableFlatList from 'react-native-draggable-flatlist'
import DateTimePicker from "react-native-modal-datetime-picker";
import Dialog, { DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import moment from 'moment';
import PushNotification from "react-native-push-notification";
import { openDatabase } from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-community/async-storage';

var db = openDatabase({ name: 'TaskGrabbnGo.db' });


export default class HomeComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isVisible: false,
            visibleDialog: false,
            status: '',
            isDateTimePickerVisible: false,
            date: '',
            task: '',
            desc: '',
            dialogIndex: 0,
            tasks: [],
            user_id_state: '',
            notification_id: 0,
            notification_id_state: '',
            date_db: '',
            refreshing: false,
            completion_check: '',
            errortask:'',
            errorDesc: '',
            errorStatus: '',
            errorDate: ''
        }
        this.onTaskEditHandle = (task) => this.setState({ task });
        this.onDescriptionEditHandle = (desc) => this.setState({ desc });

        PushNotification.configure({
            onNotification: function(notification) {
                const { data } = notification;
        
                props.navigation.navigate('Home', { notificationData: data });
            }
        });
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        console.disableYellowBox = true;

        this.getDatafromDatabse()
    }

    retrieveItem() {
        let colors = ['#2c9ae8','#7cd9d6']
        const darggable_Data = this.state.tasks.map((d, index) => ({
            key: `item-${index}`,
            label: d.task,
            desc: d.desc,
            status: d.status,
            date_time: d.date_time,
            completion_status: d.completion_status,
            user_id: d.user_id,
            notification_id: d.notification_id,
            backgroundColor: colors[index % colors.length]
        }))
        this.setState({ data: darggable_Data })
    }

    getDatafromDatabse() {
        this.setState({refreshing: !this.state.refreshing})
        db.transaction((tx) => {
            tx.executeSql('SELECT * FROM table_tasks', [], (tx, results) => {
              var temp = [];
              for (let i = 0; i < results.rows.length; ++i) {
                temp.push(results.rows.item(i));
              
              }

              for (let index = 0; index < temp.length; index++) {
                  const element = temp[index];
                  
                  if(element.date_time <= moment(new Date()).format('MMMM Do YYYY, h:mm a')) {
                      element.completion_status = "Completed"
                  }
                  }

              this.setState({tasks: temp})
              this.retrieveItem();
              this.setState({refreshing: !this.state.refreshing})
            });
          });
    }

    OpenDialog = (index, user_id, notification_id,completion_status) => {
        this.setState({visibleDialog: true,dialogIndex: index,user_id_state: user_id, notification_id_state: notification_id, completion_check: completion_status})
    }

    renderItem = ({ item, index, drag, isActive }) => {
        return (
            <TouchableOpacity style={{ height: 120, backgroundColor: isActive ? "#9c9944" : item.backgroundColor, marginBottom:5 }} onPress={() => this.OpenDialog(index,item.user_id, item.notification_id, item.completion_status)} onLongPress={drag}>

                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>
                    <Text style={{ fontWeight: "bold", color: "white", fontSize: 20, marginLeft: 20 }}>{item.label}</Text>
                    <Text style={{ fontWeight: "bold", color: "white", fontSize: 16, alignSelf: "flex-start", marginRight: 20 }}>{item.status}</Text>
                </View>

                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>
                    <Text numberOfLines={1} style={{ fontWeight: "bold", width: "90%", color: "white", fontSize: 20, marginLeft: 20,marginRight:10,marginBottom:10 }}>{item.desc}</Text>
                    
                </View>
                {item.completion_status !== '' ? 
                    (<Text style={{ fontWeight: "bold", color: "white", fontSize: 16, alignSelf: "flex-end", marginRight: 20,marginBottom:10}}>{item.completion_status}</Text>) :
                    (<Text style={{ fontWeight: "bold", color: "white", fontSize: 16, alignSelf: "flex-end", marginRight: 20,marginBottom:10 }}>Due: {item.date_time}</Text>)
                    }

            </TouchableOpacity>
        );
    };

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
         let date_db = moment(date).format('MMMM Do YYYY, h:mm a')

        let date_str = date.toUTCString()

        this.setState({date: date_str,date_db: date_db,isDateTimePickerVisible: false})
    };

    removeTask = () => {

        db.transaction((tx) => {
            tx.executeSql(
              'DELETE FROM  table_tasks where user_id=?',
              [this.state.user_id_state],
              (tx, results) => {
                if (results.rowsAffected > 0) {
                    this.refs.toast.show("Task deleted successfully", 2000)
                  this.setState({visibleDialog: false})
                } else {
                    this.refs.toast.show("Please insert a valid User Id", 2000)
                }
              }
            );
          });
          this.cancelScheduledNotifications(this.state.notification_id_state)
          this.getDatafromDatabse();
    }

    markComplete = () => {

        db.transaction((tx) => {
            tx.executeSql(
              'UPDATE table_tasks set completion_status=? where user_id=?',
              ['Completed', this.state.user_id_state],
              (tx, results) => {
                if (results.rowsAffected > 0) {
                    this.refs.toast.show("Task Updated successfully", 2000)
                  this.setState({visibleDialog: false})
                } else {
                    this.refs.toast.show("Updation Failed", 2000)
                }
              }
            );
          });
          this.cancelScheduledNotifications(this.state.notification_id_state)
          this.getDatafromDatabse();
    }

    onTaskSubmit = () => {
        if (this.state.task == '') {
            
            this.setState({errorTask: 'Task should not be empty'})
        }
        else if (this.state.desc == '') {
            
            this.setState({errorTask: '',errorDesc: 'Description should not be empty'})
        }
        else if (this.state.status == '') {
            
            this.setState({errorTask: '',errorDesc: '',errorStatus: 'Status must be selected'})
        }
        else if (this.state.date == '') {
            
            this.setState({errorTask: '',errorDesc: '',errorStatus: '',errorDate: 'Date must be selected'})
        }
        else {
            this.setState({ isVisible: false })
            let task_const = {}
            task_const.task = this.state.task,
                task_const.desc = this.state.desc,
                task_const.status = this.state.status,
                task_const.date_time = this.state.date,
                task_const.completion_status = '',
                task_const.notification_id = this.state.notification_id.toString();

            this.state.task = ''
            this.state.desc = ''
            this.state.status = ''
            this.state.date = ''
            
            let date_db =  moment(task_const.date_time).format('MMMM Do YYYY, h:mm a')

            db.transaction(function (tx) {
                tx.executeSql(
                  'INSERT INTO table_tasks (task, desc, status, date_time, completion_status, notification_id) VALUES (?,?,?,?,?,?)',
                  [task_const.task, task_const.desc, task_const.status, date_db, task_const.completion_status, task_const.notification_id],
                  (tx, results) => {
                    if (results.rowsAffected > 0) {
                      this.refs.toast.show("Task added successfully", 2000)
                    } else {
                        this.refs.toast.show("Registration Failed", 2000)
                    }
                  }
                );
              });
              var conv_date = new Date(task_const.date_time)
              this.testSchedule(task_const.task,conv_date, task_const.notification_id)
              this.setState({notification_id: this.state.notification_id + 1})
              this.getDatafromDatabse();
        }
    }

    testSchedule = (task_name,time, id) => {
        PushNotification.localNotificationSchedule({
            //... You can use all the options from localNotifications
            id: id,
            message: task_name+" will complete in next 5 mins", // (required)
            date: new Date(time - (300 * 1000)), // in before 5 mins of completion of the task
          });
    }

    cancelScheduledNotifications = (id) => {
        PushNotification.cancelLocalNotifications({ id: id});
    }

    Logout = () => {
        this.props.navigation.replace('Login')
        AsyncStorage.setItem('Logged', '')
    }

    render() {
        return (
            <Container style={HomeContainer.ContainerBG}>
                <Toast ref="toast"
                    style={{ backgroundColor: '#000' }}
                    position={"bottom"}
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
                <Header style={HomeContainer.HeaderStyle}>

                    <Text style={{ fontSize: 20, color: "black", marginRight: 'auto', marginLeft: 20 }}>Home</Text>

                </Header>
                
                {this.state.data != undefined && this.state.data != '' ?
                (<DraggableFlatList
                    data={this.state.data}
                    renderItem={this.renderItem}
                    refreshControl={
                        <RefreshControl refreshing={this.state.refreshing} onRefresh={() => this.getDatafromDatabse}/>
                    }
                    keyExtractor={(item, index) => `draggable-item-${item.key}`}
                    onDragEnd={({ data }) => this.setState({ data })}
                />) :

                (<Text style={{ fontSize: 18, color: "black", textAlign: "center",justifyContent:"center",marginTop:'50%' }}>No Tasks Found..Add New</Text>)}

                <Footer style={HomeContainer.footer}>
                    <TouchableOpacity onPress={() => this.setState({ isVisible: true })}>
                        <Text style={{ fontSize: 18, color: "black", textAlign: "center",marginRight: 30 }}>Add Task</Text>
                    </TouchableOpacity>

                    <View style={{borderWidth:0.5,borderColor:"black", width:1, height:"100%"}}/>

                    <TouchableOpacity onPress={this.Logout}>
                        <Text style={{ fontSize: 18, color: "black", textAlign: "center",marginLeft:30 }}>Logout</Text>
                    </TouchableOpacity>
                </Footer>


                <Dialog
                    visible={this.state.visibleDialog}
                    width="85%"
                    footer={
                        <DialogFooter>
                            <DialogButton
                                text="Delete Task"
                                onPress={() => this.removeTask(this.state.dialogIndex)}
                            />
                            {this.state.completion_check == '' ?
                            (<DialogButton
                                text="Mark as a Complete"
                                textStyle={{marginLeft:5,textAlign:"center"}}
                                onPress={() => this.markComplete(this.state.dialogIndex)}
                            />) : (null)}
                            <DialogButton
                                text="Close"
                                textStyle={{marginLeft:5,textAlign:"center"}}
                                onPress={() => this.setState({visibleDialog: false})}
                            />
                        </DialogFooter>
                    }
                >
                    <DialogContent>
                    <Text style={{ fontSize: 18, color: "black", textAlign: "center", marginTop:30 }}>Do you want to mark the changes in the task?</Text>
                    </DialogContent>
                </Dialog>

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.isVisible}
                    onRequestClose={() => { this.setState({ isVisible: false }) }}>
                    {/*All views of Modal*/}

                    <View style={{
                        padding: 20,
                        backgroundColor: "white",
                        width: "100%",
                        height: "100%",
                        alignItems: "center",
                        position: "absolute"
                    }}>

                        <View style={HomeContainer.text_input_bor}>
                            <TextInput
                                style={HomeContainer.text_input}
                                autoCapitalize={'none'}
                                editable={true}
                                value={this.state.task}
                                blurOnSubmit={false}
                                onChangeText={this.onTaskEditHandle}
                                returnKeyType={'next'}
                                placeholderTextColor={'black'}
                                placeholder='Task'
                            />
                        </View>
                        <Text style={{color:'red'}}>{this.state.errorTask}</Text>
                        <View style={HomeContainer.text_input_bor}>
                            <TextInput
                                style={HomeContainer.text_input}
                                autoCapitalize={'none'}
                                editable={true}
                                value={this.state.desc}
                                blurOnSubmit={false}
                                onChangeText={this.onDescriptionEditHandle}
                                returnKeyType={'next'}
                                placeholderTextColor={'black'}
                                placeholder='Description'
                            />
                        </View>
                        <Text style={{color:'red'}}>{this.state.errorDesc}</Text>

                        <View style={HomeContainer.SubView1}>

                            <Picker
                                iosHeader="Select Status"
                                iosIcon={<Icon name="chevron-down" size={12} style={{ marginLeft: 40 }} />}
                                selectedValue={this.state.status}
                                style={HomeContainer.CountryPicker}
                                onValueChange={(item) => this.setState({ status: item })} >

                                <Picker.Item value="" label="Select Status" />

                                <Picker.Item value="Urgent" label="Urgent" />
                                <Picker.Item value="Some Other Day" label="Some Other Day" />
                            </Picker>
                            <Text style={{color:'red'}}>{this.state.errorStatus}</Text>
                        </View>

                        <TouchableOpacity onPress={this.showDateTimePicker} style={HomeContainer.text_input_bor}>
                            <Text style={{color:"black"}}>{this.state.date == '' ? "Select date" : this.state.date_db}</Text>
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                mode="datetime"
                                minimumDate={new Date()}
                                isDarkModeEnabled={true}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                            />
                        </TouchableOpacity>
                        <Text style={{color:'red'}}>{this.state.errorDate}</Text>

                        <TouchableOpacity activeOpacity={0.5} onPress={this.onTaskSubmit}>
                            <View style={HomeContainer.ButtonView}>
                                <Text style={HomeContainer.ButtonText}>Add Task</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </Modal>

            </Container>
        )
    }
}