import React, { Component } from 'react';
import { Text, TextInput, View, TouchableOpacity, ScrollView, SafeAreaView, Keyboard, BackHandler } from 'react-native';
import { Container, Input } from 'native-base';
import LoginContainer from "../Containers/LoginContainer"
import Toast from 'react-native-easy-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import firebase from '../database/firebase';
import { openDatabase } from 'react-native-sqlite-storage';
import AsyncStorage from "@react-native-community/async-storage";

var db = openDatabase({ name: 'TaskGrabbnGo.db' });
export default class Auth extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            spinner: true,
            login_status: ''
        }
        this.onLoginIdEditHandle = (email) => this.setState({ email });
        this.onPasswordEditHandle = (password) => this.setState({ password });
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        console.disableYellowBox = true;

        this.createDatabse();
        this.setPath()
    }

    createDatabse = () => {
        db.transaction(function (txn) {
            txn.executeSql(
              "SELECT name FROM sqlite_master WHERE type='table' AND name='table_tasks'",
              [],
              function (tx, res) {
                if (res.rows.length == 0) {
                  txn.executeSql('DROP TABLE IF EXISTS table_tasks', []);
                  txn.executeSql(
                    'CREATE TABLE IF NOT EXISTS table_tasks(user_id INTEGER PRIMARY KEY AUTOINCREMENT, task VARCHAR(20), desc VARCHAR(100), status VARCHAR(20), date_time DATETIME, completion_status VARCHAR(20), notification_id VARCHAR(20))',
                    []
                  );
                }
              }
            );
          });
    }
    async setPath() {
        const Login_status = await AsyncStorage.getItem('Logged');
        this.setState({login_status: Login_status})
        setTimeout(() => {
       
        {this.state.login_status != null ? 
            (this.props.navigation.replace('Home')) : (this.props.navigation.replace('Login'))}

        }, 2000);
      }

    render() {
        return (
            <Container style={LoginContainer.ContainerBG}>
                <Toast ref="toast"
                    style={{ backgroundColor: '#000' }}
                    position={"bottom"}
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Please wait...'}
                    textStyle={LoginContainer.spinnerTextStyle}
                />
            </Container>
        )
    }
}
