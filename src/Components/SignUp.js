import React, { Component } from 'react';
import { Text, TextInput, View, TouchableOpacity, ScrollView, SafeAreaView, Keyboard, BackHandler } from 'react-native';
import { Container } from 'native-base';
import LoginContainer from "../Containers/LoginContainer"
import Toast from 'react-native-easy-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import firebase from '../database/firebase';

export default class SignUpComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            cPassword: '',
            erroMessage: '',
            spinner: false
        }
        this.onNameEditHandle = (name) => this.setState({ name });
        this.onLoginIdEditHandle = (email) => this.setState({ email });
        this.onPasswordEditHandle = (password) => this.setState({ password });
        this.oncPasswordEditHandle = (cPassword) => this.setState({ cPassword });
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        console.disableYellowBox = true;
    }

    onSignUpSubmit = () => {
        if (this.state.name == '') {
            this.refs.toast.show("Name should not be empty", 2000)
        }
        else if (this.state.email == '') {
            this.refs.toast.show("Email should not be empty", 2000)
        }
        else if (this.state.password == '') {
            this.refs.toast.show("Password should not be empty", 2000)
        }
        else if (this.state.cPassword !== this.state.password) {
            this.refs.toast.show("Password and Confirm password should be match", 2000)
        }
        else {
            this.setState({ spinner: true })
            
                
                firebase
                .auth()
                .createUserWithEmailAndPassword(this.state.email, this.state.password)
                .then((res) => {
                    this.setState({ spinner: false })
                    res.user.updateProfile({
                        displayName: this.state.name
                    })
                    this.refs.toast.show("SignUp Successfully", 2000)
                    setTimeout(() => {
                    
                    this.props.navigation.navigate('Login')
                }, 3000);
                }).catch((error) => {
                this.setState({spinner: false}),
                this.refs.toast.show("Something went wrong!", 2000)
                })
            
        }
    }

    render() {
        return (
            <Container style={LoginContainer.ContainerBG}>
                <Toast ref="toast"
                    style={{ backgroundColor: '#000' }}
                    position={"bottom"}
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Please wait...'}
                    textStyle={LoginContainer.spinnerTextStyle}
                />
                <ScrollView>
                    <View style={LoginContainer.CenterView}>
                        <Text style={{ fontSize: 40, alignSelf: 'center', marginBottom: 20, color: "white" }}>Sign Up</Text>

                        <View style={LoginContainer.SubView_1}>
                            <TextInput
                                style={LoginContainer.SubText_1}
                                autoCapitalize={'none'}
                                editable={true}
                                value={this.state.LoginId}
                                blurOnSubmit={false}
                                onChangeText={this.onNameEditHandle}
                                returnKeyType={'next'}
                                placeholderTextColor={'white'}
                                placeholder='Name'
                            />
                        </View>
                        <View style={LoginContainer.SubView_1}>
                            <TextInput
                                style={LoginContainer.SubText_1}
                                autoCapitalize={'none'}
                                editable={true}
                                value={this.state.LoginId}
                                blurOnSubmit={false}
                                onChangeText={this.onLoginIdEditHandle}
                                returnKeyType={'next'}
                                placeholderTextColor={'white'}
                                placeholder='Email address'
                            />
                        </View>

                        <View style={LoginContainer.SubView_1}>

                            <TextInput
                                style={LoginContainer.SubText_1}
                                autoCapitalize={'none'}
                                editable={true}
                                secureTextEntry={true}
                                value={this.state.password}
                                blurOnSubmit={false}
                                onChangeText={this.onPasswordEditHandle}
                                returnKeyType={'done'}
                                placeholderTextColor={"white"}
                                placeholder='Password'
                            />
                        </View>

                        <View style={LoginContainer.SubView_1}>

                            <TextInput
                                style={LoginContainer.SubText_1}
                                autoCapitalize={'none'}
                                editable={true}
                                secureTextEntry={true}
                                value={this.state.cPassword}
                                blurOnSubmit={false}
                                onChangeText={this.oncPasswordEditHandle}
                                returnKeyType={'done'}
                                placeholderTextColor={"white"}
                                placeholder='Confirm Password'
                            />
                        </View>
                    </View>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.onSignUpSubmit}>
                        <View style={LoginContainer.ButtonView}>
                            <Text style={LoginContainer.ButtonText}>Sign Up</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                    <Text style={LoginContainer.SignUpNavigationText}>Have an account? Login</Text>
                    </TouchableOpacity>
                </ScrollView>
            </Container>
        )
    }
}
