import React, { Component } from 'react';
import { Text, TextInput, View, TouchableOpacity, ScrollView, SafeAreaView, Keyboard, BackHandler } from 'react-native';
import { Container, Input } from 'native-base';
import LoginContainer from "../Containers/LoginContainer"
import Toast from 'react-native-easy-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import firebase from '../database/firebase';
import { openDatabase } from 'react-native-sqlite-storage';
import AsyncStorage from "@react-native-community/async-storage";

var db = openDatabase({ name: 'TaskGrabbnGo.db' });
export default class LoginComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            spinner: false
        }
        this.onLoginIdEditHandle = (email) => this.setState({ email });
        this.onPasswordEditHandle = (password) => this.setState({ password });
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        console.disableYellowBox = true;

        //this.createDatabse();
    }

    createDatabse = () => {
        db.transaction(function (txn) {
            txn.executeSql(
              "SELECT name FROM sqlite_master WHERE type='table' AND name='table_tasks'",
              [],
              function (tx, res) {
                if (res.rows.length == 0) {
                  txn.executeSql('DROP TABLE IF EXISTS table_tasks', []);
                  txn.executeSql(
                    'CREATE TABLE IF NOT EXISTS table_tasks(user_id INTEGER PRIMARY KEY AUTOINCREMENT, task VARCHAR(20), desc VARCHAR(100), status VARCHAR(20), date_time DATETIME, completion_status VARCHAR(20), notification_id VARCHAR(20))',
                    []
                  );
                }
              }
            );
          });
    }

    onLoginSubmit = () => {
        if (this.state.email == '') {
            this.refs.toast.show("Email should not be empty", 2000)
        }
        else if (this.state.password == '') {
            this.refs.toast.show("Password should not be empty", 2000)
        }
        else {
            this.setState({ spinner: true })
                firebase
                .auth()
                .signInWithEmailAndPassword(this.state.email,this.state.password)
                .then((res) => {
                    this.setState({ spinner: false })
                    this.refs.toast.show("Login Successfully", 2000)

                    AsyncStorage.setItem('Logged', 'LoggedIn')


                    setTimeout(() => {
                        this.props.navigation.replace('Home')  
                    }, 4000);  
                }).catch((error) => {
                    this.setState({ spinner: false })
                    this.refs.toast.show("Bad credentials or Network issue", 2000)
                })
        }
    }

    render() {
        return (
            <Container style={LoginContainer.ContainerBG}>
                <Toast ref="toast"
                    style={{ backgroundColor: '#000' }}
                    position={"bottom"}
                    positionValue={200}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: '#fff' }}
                />
                <Spinner
                    visible={this.state.spinner}
                    textContent={'Please wait...'}
                    textStyle={LoginContainer.spinnerTextStyle}
                />
                <ScrollView>
                    <View style={LoginContainer.CenterView}>
                        <Text style={{ fontSize: 40, alignSelf: 'center', marginBottom: 20, color: "white" }}>Login</Text>
                        <View style={LoginContainer.SubView_1}>
                            <TextInput
                                style={LoginContainer.SubText_1}
                                autoCapitalize={'none'}
                                editable={true}
                                value={this.state.LoginId}
                                blurOnSubmit={false}
                                onChangeText={this.onLoginIdEditHandle}
                                returnKeyType={'next'}
                                placeholderTextColor={'white'}
                                placeholder='Email address'
                            />
                        </View>

                        <View style={LoginContainer.SubView_1}>

                            <TextInput
                                style={LoginContainer.SubText_1}
                                autoCapitalize={'none'}
                                editable={true}
                                secureTextEntry={true}
                                value={this.state.Password}
                                blurOnSubmit={false}
                                onChangeText={this.onPasswordEditHandle}
                                returnKeyType={'done'}
                                placeholderTextColor={"white"}
                                placeholder='Password'
                            />
                        </View>
                    </View>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.onLoginSubmit}>
                        <View style={LoginContainer.ButtonView}>
                            <Text style={LoginContainer.ButtonText}>LOG IN</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup')}>
                    <Text style={LoginContainer.SignUpNavigationText}>Don't have an account? SignUp</Text>
                    </TouchableOpacity>
                </ScrollView>
            </Container>
        )
    }
}
