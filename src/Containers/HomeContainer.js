import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
    ContainerBG: { backgroundColor: '#e5e5e5' },
    CardView: { marginTop: 10, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#fff' },
    HeaderStyle: {
        ...Platform.select({
            android: {
                backgroundColor: '#ffffff',
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
            },
            ios: {
                backgroundColor: '#ffffff',
                height: 45,
            }
        })
    },
    floatButton:{
        flex:1,
        top:48,
        left:50,
        alignSelf:'center',
        backgroundColor:'blue'
      },
      footer:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
      },
      SubView1: {
        ...Platform.select({
            ios: {
                height: 40,
            }
        }),
        borderWidth: 0.5,borderColor:"black", marginTop: 18, width: '75%', alignSelf: 'center', marginBottom: 18
    },
    CountryPicker: {
        ...Platform.select({
            android: {
                marginLeft: 0.5,
                marginRight: 0.5,
                //alignItems: 'flex-start',
                color: 'black',
            },
            ios: {
                marginLeft: 0.5,
                marginRight: 0.5,
                color: 'black',
                //alignItems: 'flex-start',
            }
        })

    },
    text_input_bor: {
        height: Platform.OS === 'ios' ? 50 : 45,
        flexDirection: 'row', borderWidth: 0.5, borderColor: "black", alignItems: 'center', marginTop: 20, width: '75%', alignSelf: 'center'
    },
    text_input: { height: '100%', fontSize: 14, marginLeft: 10, width: '74%', color: 'black', },
    ButtonView: { width: '90%', alignItems: 'center', backgroundColor: '#C82257', alignSelf: 'center', height: 40, justifyContent: 'center', marginTop: 30 },
    ButtonText: { color: 'white', fontSize: 16, fontWeight: 'bold',marginLeft:30,marginRight: 30 },

})