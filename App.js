/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Auth from './src/Components/Auth';
import Login from './src/Components/Login';
import SignUp from './src/Components/SignUp';
import Home from './src/Components/Home';
import PushNotification from "react-native-push-notification";


const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      initialRouteName="Auth"
      headerMode="none"
      >
        <Stack.Screen 
        name="Auth" 
        component={Auth} 
      />
        <Stack.Screen 
        name="Login" 
        component={Login} 
      />
      <Stack.Screen 
        name="Signup" 
        component={SignUp} 
      />   
      <Stack.Screen 
        name="Home" 
        component={Home} 
      />       
    </Stack.Navigator>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    PushNotification.configure({
      onRegister: function (token) {
        console.log("TOKEN:", token);
      },
     
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
     
     
        notification.finish();
      },
     
      onAction: function (notification) {
        console.log("ACTION:", notification.action);
        console.log("NOTIFICATION:", notification);
      },
     
      onRegistrationError: function(err) {
        console.error(err.message, err);
      },
        permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,         
      requestPermissions: true,
    });
  }

  render() {
    return (
      <NavigationContainer>
        <MyStack/>
        </NavigationContainer>
    )
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
